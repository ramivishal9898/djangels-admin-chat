import axios from 'axios';

let baseURL = process.env.baseURL || "#";

let instance = axios.create({
	baseURL
});

export const HTTP = instance;